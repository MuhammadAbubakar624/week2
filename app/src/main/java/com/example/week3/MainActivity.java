package com.example.week3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button submit;
    EditText username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        submit = findViewById(R.id.button);
        username = findViewById(R.id.editTextTextPersonName2);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                if (username.getText().toString().equals("F180207")) {
                    Toast.makeText(getApplicationContext(), "Correct Username", Toast.LENGTH_SHORT).show();
                }
                else {
                    alert   .setTitle("Alert")
                            .setMessage("Invalid Username")
                            .setNegativeButton("Close", null)
                            .show();
                }
            }
        });
    }
}